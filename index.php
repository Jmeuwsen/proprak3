<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="scss/style.css">
    
  </head>
  <body>
    <div id="mySidebar" class="sidebar">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">X</a> 
      <a href="project.html"><img src="images/logo_small_icon_only.png"></a> 
      <a href="opleidingsinformatie.html">Opleidingsinfo</a>
      <a href="./archief.php">Archiefpagina</a> 
      <a href="contact.html">Contact</a> 
      <a href="FAQ.html">FAQ</a> 
    </div>
    <div id="main"> 
      <button class="openbtn" onclick="openNav()">Menu</button>
      <div class="center">
        <h3 style="color:white">Welkom</h3>
        <p>Op deze website zult u meer informatie vinden over de opleiding Software Developer niveau 4 aan het MBO Utrecht</p>
      </div>  
    </div>
    <footer>
      <strong>Roddels met Barry B Inc. 2022&copy;</strong>
    </footer>
    <script src="js/script.js"></script>
  </body>
</html>