<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/grid.css">
        <link rel="stylesheet" href="scss/style.css">
        <link rel="stylesheet" href="css/style.css">
        <title>Home</title>
    </head>
    <body>
        <header>
            <nav>
            <div id="mySidenav" class="sidenav">
                <ul class="menu-hover-fill flex flex-col items-start leading-none text-2xl uppercase space-y-4">
                    <li><a href="#" data-text="home">home</a></li>
                    <li><a href="#" data-text="archives">archives</a></li>
                    <li><a href="#" data-text="tags">tags</a></li>
                    <li><a href="#" data-text="categories">categories</a></li>
                    <li><a href="#" data-text="about">about</a></li>
                </ul>
            </div>
                <div class=" icon nav-icon-5" onclick="toggleNav()">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <img class="header_img" src="images/logo_small.png" alt="">
            </nav>
        </header>
        <div class="curves">
            <svg class="topcurves" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet">
                    <path d="M0, 100 C150, 200 350,
                        0 500, 100 L500, 00 L0, 0 Z">
                    </path>
                </svg>
            </div>           
            <div class="curves2">
                <svg viewBox="0 0 500 500"
                    preserveAspectRatio="xMinYMin meet">
                    
                    <path d="M0, 80 C300, 0 400,
                        300 500, 50 L500, 00 L0, 0 Z">
                    </path>
                </svg>
            </div>           
            <div class="curves3">
                <svg viewBox="0 0 500 500"
                    preserveAspectRatio="xMinYMin meet">                   
                    <path d="M0, 100 C150, 300 350,
                        0 500, 100 L500, 00 L0, 0 Z"
                        style="stroke: none;
                        fill:color(curve-light);">
                    </path>
                </svg>
            </div>
        </div>
        <main>
            <div class="container">
                <div class="row">
                <div class="title-style-2 col-12">
                    <h2>Ons archief</h2>
                </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <ul class="cards">
                            <li class="cards_item">
                                <div class="card">
                                <img class="card_image" src="images/logo_small.png" alt="">
                                <div class="card_content">
                                    <div class="card_title">Dj stiekem geen Dj?!</div>
                                    <p class="card_text">maar wie is hij dan wel?</p>
                                    <button class="btn btn-block card_btn" href="">Meer lezen</button>
                                </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="cards">
                            <li class="cards_item">
                                <div class="card">
                                <img class="card_image" src="images/logo_small.png" alt="">
                                <div class="card_content">
                                    <div class="card_title">Claudia’s geheime routine 18+ Gaat fout!?</div>
                                    <p class="card_text"></p>
                                    <button class="btn btn-block card_btn" href="">Meer lezen</button>
                                </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="cards">
                            <li class="cards_item">
                                <div class="card">
                                <img class="card_image" src="images/logo_small.png" alt="">
                                <div class="card_content">
                                    <div class="card_title">Berend gay??</div>
                                    <p class="card_text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus perferendis aperiam voluptatem voluptatibus voluptatum. Aliquid ducimus culpa qui error assumenda dolor cum dolores numquam maxime iusto. Error veritatis voluptate praesentium.</p>
                                    <button class="btn btn-block card_btn" href="">Meer lezen</button>
                                </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <ul class="cards">
                            <li class="cards_item">
                                <div class="card">
                                <img class="card_image" src="images/logo_small.png" alt="">
                                <div class="card_content">
                                    <div class="card_title">Sander heet eigenlijk ALEXander?!?!</div>
                                    <p class="card_text"></p>
                                    <button class="btn btn-block card_btn" href="">Meer lezen</button>
                                </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="cards">
                            <li class="cards_item">
                                <div class="card">
                                <img class="card_image" src="images/logo_small.png" alt="">
                                <div class="card_content">
                                    <div class="card_title">Dj = jesse in disguise??</div>
                                    <p class="card_text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur sapiente ipsam, culpa quia qui tempore nesciunt alias dolorum accusamus suscipit officia recusandae, quod corrupti totam inventore quibusdam sequi. Sint, unde?</p>
                                    <button class="btn btn-block card_btn" href="">Meer lezen</button>
                                </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="cards">
                            <li class="cards_item">
                                <div class="card">
                                <img class="card_image" src="images/logo_small.png" alt="">
                                <div class="card_content">
                                    <div class="card_title">Roddel 6</div>
                                    <p class="card_text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deleniti fugiat consequatur tenetur fuga dolor mollitia corporis commodi assumenda deserunt. Culpa quam repudiandae ipsam quia molestias quisquam maxime eum in neque.</p>
                                    <button class="btn btn-block card_btn" href="">Meer Lezen</button>
                                </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </main>
        <footer class="fixed_footer">
            <div class="content">
                <p>Roddels met Barry B Inc. 2022&copy;</p>
            </div>
        </footer>
        <script src="js/ham.js"></script>
        <!-- <script src="js/script.js"></script> -->
    </body>
</html>
